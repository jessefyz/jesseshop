# jesseshop

#### 介绍
单商户商城开发脚手架
后台使用：若依分离版（在此基础对框架添加了若干组件：mybatis-plus,es,magic-api）
小程序端：litemall小程序，
！支持docker部署，更换了swagger，使用knife4j,界面更加清晰
#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/174935_d5dfa223_924029.png "屏幕截图.png")


#### 安装教程

1.  使用idea将AdminApplication和admin-ui跑起来
    账号：admin   密码：admin123
2.  后台admin接口文档地址：http://localhost:8080/admin/doc.html
3.  api有自己的鉴权体系，详细可以看com.jessefyz.module.interceptor.AuthInterceptor的鉴权代码

#### 后台
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180122_affce7fe_924029.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180143_d03719a1_924029.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180218_e8781ffe_924029.png "屏幕截图.png")
![输入图片说明](1.png)
![输入图片说明](2.png)
![输入图片说明](3.png)
![输入图片说明](4.png)
![输入图片说明](5.png)
![输入图片说明](6.png)
![输入图片说明](7.png)
![输入图片说明](8.png)

#### 小程序端
![输入图片说明](storage/11.png)
![输入图片说明](storage/12.png)

#### 接口文档
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180254_7bf25789_924029.png "屏幕截图.png")

#### 参与贡献

1.  若依前后端分离版本
2.  litemall商城
3.  magic-api



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)