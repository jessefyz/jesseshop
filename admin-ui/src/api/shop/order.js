import request from '@/utils/request'

// 查询订单列表
export function listOrder(query) {
  return request({
    url: '/shop/order/list',
    method: 'get',
    params: query
  })
}

// 查询订单详细
export function getOrder(id) {
  return request({
    url: '/shop/order/' + id,
    method: 'get'
  })
}

// 新增订单
export function addOrder(data) {
  return request({
    url: '/shop/order',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateOrder(data) {
  return request({
    url: '/shop/order',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delOrder(id) {
  return request({
    url: '/shop/order/' + id,
    method: 'delete'
  })
}

// 导出订单
export function exportOrder(query) {
  return request({
    url: '/shop/order/export',
    method: 'get',
    params: query
  })
}

// 发货渠道
export function listChannel(id) {
  return request({
    url: '/shop/order/channel',
    method: 'get'
  })
}

// 发货提交
export function shipOrder(data) {
  return request({
    url: '/shop/order/ship',
    method: 'post',
    data
  })
}

// 收款提交
export function payOrder(data) {
  return request({
    url: '/shop/order/pay',
    method: 'post',
    data
  })
}

// 查看物流
export function getOrderExpress(data) {
  return request({
    url: '/shop/order/express',
    method: 'post',
    data
  })
}

// 查看物流
export function refundOrder(data) {
  return request({
    url: '/shop/order/refund',
    method: 'post',
    data
  })
}
