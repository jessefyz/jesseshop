package com.jessefyz.module.controller.member;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.List;
import java.util.Arrays;

import com.jessefyz.common.exception.ParamException;
import com.jessefyz.module.member.domain.vo.MemberInfoVo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jessefyz.common.annotation.Log;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.enums.BusinessType;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.member.domain.req.MemberInfoReq;
import com.jessefyz.module.member.service.IMemberInfoService;
import com.jessefyz.common.utils.poi.ExcelUtil;
import javax.validation.Valid;
import io.swagger.annotations.*;
import com.jessefyz.common.core.page.TableDataInfo;
/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName MemberInfoController
 * @Author jessefyz
 * @Date 2020-11-06
 * @Version 1.0.0
 * @Description 会员信息Controller
 */
@Validated
@Api(tags = "会员信息")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/member/memberInfo" )
public class MemberInfoController extends BaseController {

    private final IMemberInfoService iMemberInfoService;

 /**
  * @Description 分页
  * @Author jessefyz
  * @Date 2020-11-06
  * @Param memberInfo
  * @Return TableDataInfo
 */
    @ApiOperation("列表")
    @PreAuthorize("@ss.hasPermi('member:memberInfo:list')")
    @GetMapping("/list")
    public TableDataInfo<MemberInfoVo> list(MemberInfoReq memberInfoReq)
    {
        startPage();
        QueryWrapper<MemberInfo> lqw = new QueryWrapper<MemberInfo>();
        memberInfoReq.generatorQuery(lqw);
        List<MemberInfo> list = iMemberInfoService.list(lqw);

        List<MemberInfoVo> listMember = Convert.convert(new TypeReference<List<MemberInfoVo>>() {}, list);

        return getDataTable(listMember);
    }

    /**
     * @Description 导出
     * @Author jessefyz
     * @Date 2020-11-06
     * @Param memberInfo
     * @Return TableDataInfo
     */
    @ApiOperation("导出")
    @PreAuthorize("@ss.hasPermi('member:memberInfo:export')" )
    @Log(title = "导出会员信息" , businessType = BusinessType.EXPORT)
    @GetMapping("/export" )
    public AjaxResult export(MemberInfoReq memberInfoReq) {
        QueryWrapper<MemberInfo> lqw = new QueryWrapper<MemberInfo>();
        memberInfoReq.generatorQuery(lqw);
        List<MemberInfo> list = iMemberInfoService.list(lqw);
        ExcelUtil<MemberInfo> util = new ExcelUtil<MemberInfo>(MemberInfo. class);
        return util.exportExcel(list, "memberInfo" );
    }

    /**
     * @Description 详情
     * @Author jessefyz
     * @Date 2020-11-06
     * @Param memberInfo
     * @Return TableDataInfo
     */
    @ApiOperation("详情")
    @PreAuthorize("@ss.hasPermi('member:memberInfo:query')" )
    @GetMapping(value = "/{id}" )
    public AjaxResult<MemberInfo> getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(iMemberInfoService.getById(id));
    }

    /**
     * @Description 新增
     * @Author jessefyz
     * @Date 2020-11-06
     * @Param memberInfo
     * @Return TableDataInfo
     */
    @ApiOperation("新增")
    @PreAuthorize("@ss.hasPermi('member:memberInfo:add')" )
    @Log(title = "添加会员信息" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody MemberInfo memberInfo) {
        return toAjax(iMemberInfoService.save(memberInfo) ? 1 : 0);
    }

    /**
     * @Description 修改
     * @Author jessefyz
     * @Date 2020-11-06
     * @Param memberInfo
     * @Return TableDataInfo
     */
    @ApiOperation("修改")
    @PreAuthorize("@ss.hasPermi('member:memberInfo:edit')" )
    @Log(title = "修改会员信息" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody MemberInfo memberInfo) {
        if (null == memberInfo.getId()) {
            throw new ParamException("id不能为空");
        }
        return toAjax(iMemberInfoService.updateById(memberInfo) ? 1 : 0);
    }

    /**
     * @Description 删除
     * @Author jessefyz
     * @Date 2020-11-06
     * @Param memberInfo
     * @Return TableDataInfo
     */
    @ApiOperation("删除")
    @PreAuthorize("@ss.hasPermi('member:memberInfo:remove')" )
    @Log(title = "删除会员信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}" )
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(iMemberInfoService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
    }
}
