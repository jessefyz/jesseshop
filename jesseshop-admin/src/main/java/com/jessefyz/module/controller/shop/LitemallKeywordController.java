package com.jessefyz.module.controller.shop;

import com.jessefyz.common.annotation.Log;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.core.page.TableDataInfo;
import com.jessefyz.common.enums.BusinessType;
import com.jessefyz.common.utils.poi.ExcelUtil;
import com.jessefyz.module.shop.domain.LitemallKeyword;
import com.jessefyz.module.shop.service.ILitemallKeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 关键字Controller
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@RestController
@RequestMapping("/shop/keyword")
public class LitemallKeywordController extends BaseController
{
    @Autowired
    private ILitemallKeywordService litemallKeywordService;

    /**
     * 查询关键字列表
     */
    @PreAuthorize("@ss.hasPermi('shop:keyword:list')")
    @GetMapping("/list")
    public TableDataInfo list(LitemallKeyword litemallKeyword)
    {
        startPage();
        List<LitemallKeyword> list = litemallKeywordService.selectLitemallKeywordList(litemallKeyword);
        return getDataTable(list);
    }

    /**
     * 导出关键字列表
     */
    @PreAuthorize("@ss.hasPermi('shop:keyword:export')")
    @Log(title = "关键字", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(LitemallKeyword litemallKeyword)
    {
        List<LitemallKeyword> list = litemallKeywordService.selectLitemallKeywordList(litemallKeyword);
        ExcelUtil<LitemallKeyword> util = new ExcelUtil<LitemallKeyword>(LitemallKeyword.class);
        return util.exportExcel(list, "keyword");
    }

    /**
     * 获取关键字详细信息
     */
    @PreAuthorize("@ss.hasPermi('shop:keyword:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(litemallKeywordService.selectLitemallKeywordById(id));
    }

    /**
     * 新增关键字
     */
    @PreAuthorize("@ss.hasPermi('shop:keyword:add')")
    @Log(title = "关键字", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LitemallKeyword litemallKeyword)
    {
        return toAjax(litemallKeywordService.insertLitemallKeyword(litemallKeyword));
    }

    /**
     * 修改关键字
     */
    @PreAuthorize("@ss.hasPermi('shop:keyword:edit')")
    @Log(title = "关键字", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LitemallKeyword litemallKeyword)
    {
        return toAjax(litemallKeywordService.updateLitemallKeyword(litemallKeyword));
    }

    /**
     * 删除关键字
     */
    @PreAuthorize("@ss.hasPermi('shop:keyword:remove')")
    @Log(title = "关键字", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(litemallKeywordService.deleteLitemallKeywordByIds(ids));
    }
}
