package com.jessefyz.module.controller.shop;

import com.jessefyz.common.annotation.Log;
import com.jessefyz.common.constant.Constants;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.core.page.TableDataInfo;
import com.jessefyz.common.core.redis.RedisCache;
import com.jessefyz.common.enums.BusinessType;
import com.jessefyz.common.utils.poi.ExcelUtil;
import com.jessefyz.module.shop.domain.LitemallOrder;
import com.jessefyz.module.shop.express.ExpressService;
import com.jessefyz.module.shop.express.dao.ExpressInfo;
import com.jessefyz.module.shop.req.OrderExpress;
import com.jessefyz.module.shop.req.OrderPay;
import com.jessefyz.module.shop.req.OrderRefund;
import com.jessefyz.module.shop.req.OrderShip;
import com.jessefyz.module.shop.service.ILitemallOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 订单Controller
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Validated
@RestController
@RequestMapping("/shop/order")
public class LitemallOrderController extends BaseController
{
    @Autowired
    private ILitemallOrderService litemallOrderService;

    @Autowired
    private ExpressService expressService;

    @Autowired
    private RedisCache redisCache;

    /**
     * 查询订单列表
     */
    @PreAuthorize("@ss.hasPermi('shop:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(LitemallOrder litemallOrder)
    {
        startPage();
        List<LitemallOrder> list = litemallOrderService.selectLitemallOrderList(litemallOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('shop:order:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(LitemallOrder litemallOrder)
    {
        List<LitemallOrder> list = litemallOrderService.selectLitemallOrderList(litemallOrder);
        ExcelUtil<LitemallOrder> util = new ExcelUtil<LitemallOrder>(LitemallOrder.class);
        return util.exportExcel(list, "order");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('shop:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(litemallOrderService.selectLitemallOrderById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('shop:order:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LitemallOrder litemallOrder)
    {
        return toAjax(litemallOrderService.insertLitemallOrder(litemallOrder));
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('shop:order:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LitemallOrder litemallOrder)
    {
        return toAjax(litemallOrderService.updateLitemallOrder(litemallOrder));
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('shop:order:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(litemallOrderService.deleteLitemallOrderByIds(ids));
    }

    /**
     * 查询物流公司
     *
     * @return
     */
    @GetMapping("/channel")
    public Object channel() {
        return AjaxResult.success(expressService.getVendors());
    }

    /**
     * 发货
     * @return 订单操作结果
     */
    @PreAuthorize("@ss.hasPermi('shop:order:ship')")
    @PostMapping("/ship")
    public Object ship(@Valid @RequestBody OrderShip orderShip) {
        litemallOrderService.ship(orderShip);
        return AjaxResult.success("发货成功");
    }

    /**
     * 查看物流
     * @return 订单操作结果
     */
    @PostMapping("/express")
    public Object express(@Valid @RequestBody OrderExpress orderExpress) {
        //先从缓存拿，避免收费资源浪费
        ExpressInfo expressInfo = redisCache.getCacheObject(Constants.EXPRESS_PRE + orderExpress.getShipSn());
        if (null == expressInfo) {
            expressInfo = this.expressService.getExpressInfo(orderExpress.getShipChannel(),orderExpress.getShipSn());
            //设置五分钟过期，具体时限结合自己业务需求来
            if (expressInfo.getSuccess()) {
                redisCache.setCacheObject(Constants.EXPRESS_PRE + orderExpress.getShipSn(),expressInfo,5, TimeUnit.MINUTES);
            } else {
                return AjaxResult.error(expressInfo.getReason());
            }
        }
        return AjaxResult.success(expressInfo);
    }

    /**
     * 收款
     * @return 订单操作结果
     */
    @PreAuthorize("@ss.hasPermi('shop:order:pay')")
    @PostMapping("/pay")
    public Object pay(@Valid @RequestBody OrderPay orderPay) {
        litemallOrderService.pay(orderPay);
        return AjaxResult.success("收款成功");
    }

    /**
     * 退款
     * @return 订单操作结果
     */
    @PreAuthorize("@ss.hasPermi('shop:order:refund')")
    @PostMapping("/refund")
    public Object refund(@Valid @RequestBody OrderRefund orderRefund) {
        litemallOrderService.refund(orderRefund);
        return AjaxResult.success("审核成功");
    }
}
