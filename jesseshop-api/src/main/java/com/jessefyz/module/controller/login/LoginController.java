package com.jessefyz.module.controller.login;

import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.message.AliyunMessage;
import com.jessefyz.module.dto.WxLoginInfo;
import com.jessefyz.module.req.MiniAppLoginReq;
import com.jessefyz.module.service.IApiLoginService;
import com.jessefyz.module.vo.MemberVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName LoginController
 * @Author Jesse(aaxxxxxxxx@qq.com)
 * @Date 2020-11-06 11:00:00 星期五
 * @Version 1.0.0
 * @Description 登录相关
 */
@RequestMapping("/wx/auth")
@Api(tags = "登录相关")
@RestController
public class LoginController {

    @Autowired
    private IApiLoginService apiLoginService;

    /**
     * 小程序授权
     * @return
     */
    @ApiOperation(value = "小程序登陆")
    @PostMapping("/loginBywechatMiniApp")
    public AjaxResult<MemberVo> loginBywechatMiniApp(@RequestBody @Valid WxLoginInfo wxLoginInfo,
                                                     HttpServletRequest request
                                                     ) throws Exception  {
        return apiLoginService.loginBywechatMiniApp(wxLoginInfo,request);
    }

    @Resource
    private AliyunMessage aliyunMessage;
    /**
     * 小程序授权
     * @return
     */
    @ApiOperation(value = "发送短信验证码")
    @ApiImplicitParam(name = "mobile",value = "手机号码",required = true,paramType = "query")
    @PostMapping("/sendMessageCode")
    public AjaxResult sendMessageCode(@RequestParam(name = "mobile") String mobile) throws Exception  {

        this.aliyunMessage.alidayuCheckCode(mobile,"5684");

        return AjaxResult.success();
    }

}
