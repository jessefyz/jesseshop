package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.exception.DataNotFoundException;
import com.jessefyz.module.service.HomeCacheManager;
import com.jessefyz.module.shop.domain.LitemallCategory;
import com.jessefyz.module.shop.service.ILitemallCategoryService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类目服务
 */
@RestController
@RequestMapping("/wx/catalog")
@Validated
public class WxCatalogController {
    private final Log logger = LogFactory.getLog(WxCatalogController.class);

    @Autowired
    private ILitemallCategoryService categoryService;

    @GetMapping("/getfirstcategory")
    public AjaxResult<List<LitemallCategory>> getFirstCategory() {
        // 所有一级分类目录
        List<LitemallCategory> l1CatList = categoryService.queryL1();
        return AjaxResult.success(l1CatList);
    }

    @GetMapping("/getsecondcategory")
    public AjaxResult<List<LitemallCategory>> getSecondCategory(@NotNull Long id) {
        // 所有二级分类目录
        List<LitemallCategory> currentSubCategory = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
            .eq(LitemallCategory::getParentId,id)
        );
        return AjaxResult.success(currentSubCategory);
    }

    /**
     * 分类详情
     *
     * @param id   分类类目ID。
     *             如果分类类目ID是空，则选择第一个分类类目。
     *             需要注意，这里分类类目是一级类目
     * @return 分类详情
     */
    @GetMapping("index")
    public AjaxResult index(Long id) {

        // 所有一级分类目录
        List<LitemallCategory> l1CatList = categoryService.queryL1();

        // 当前一级分类目录
        LitemallCategory currentCategory = null;
        if (id != null) {
            currentCategory = categoryService.getById(id);
        } else {
             if (l1CatList.size() > 0) {
                currentCategory = l1CatList.get(0);
            }
        }

        // 当前一级分类目录对应的二级分类目录
        List<LitemallCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
                    .eq(LitemallCategory::getParentId,currentCategory.getId())
            );
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return AjaxResult.success(data);
    }

    /**
     * 所有分类数据
     *
     * @return 所有分类数据
     */
    @GetMapping("all")
    public AjaxResult queryAll() {
        //优先从缓存中读取
        if (HomeCacheManager.hasData(HomeCacheManager.CATALOG)) {
            return AjaxResult.success(HomeCacheManager.getCacheData(HomeCacheManager.CATALOG));
        }


        // 所有一级分类目录
        List<LitemallCategory> l1CatList = categoryService.queryL1();

        //所有子分类列表
        Map<Long, List<LitemallCategory>> allList = new HashMap<>();
        List<LitemallCategory> sub;
        for (LitemallCategory category : l1CatList) {
            sub = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
                    .eq(LitemallCategory::getParentId,category.getId())
                    .orderByAsc(LitemallCategory::getSortOrder)
            );
            allList.put(category.getId(), sub);
        }

        // 当前一级分类目录
        LitemallCategory currentCategory = l1CatList.get(0);

        // 当前一级分类目录对应的二级分类目录
        List<LitemallCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
                    .eq(LitemallCategory::getParentId,currentCategory.getId())
                    .orderByAsc(LitemallCategory::getSortOrder)
            );
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("allList", allList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        //缓存数据
        HomeCacheManager.loadData(HomeCacheManager.CATALOG, data);
        return AjaxResult.success(data);
    }

    /**
     * 当前分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("current")
    public AjaxResult current(@NotNull Long id) {
        // 当前分类
        LitemallCategory currentCategory = categoryService.getById(id);
        if(currentCategory == null){
            throw new DataNotFoundException("数据不存在");
        }
        List<LitemallCategory> currentSubCategory = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
                .eq(LitemallCategory::getParentId,currentCategory.getId())
                .orderByAsc(LitemallCategory::getSortOrder)
        );
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return AjaxResult.success(data);
    }
}
