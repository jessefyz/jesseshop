package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.JacksonUtil;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.domain.LitemallCollect;
import com.jessefyz.module.shop.domain.LitemallGoods;
import com.jessefyz.module.shop.domain.LitemallTopic;
import com.jessefyz.module.shop.service.ILitemallCollectService;
import com.jessefyz.module.shop.service.ILitemallGoodsService;
import com.jessefyz.module.shop.service.ILitemallTopicService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户收藏服务
 */
@RestController
@RequestMapping("/wx/collect")
@Validated
public class WxCollectController {
    private final Log logger = LogFactory.getLog(WxCollectController.class);

    @Autowired
    private ILitemallCollectService collectService;
    @Autowired
    private ILitemallGoodsService goodsService;
    @Autowired
    private ILitemallTopicService topicService;

    /**
     * 用户收藏列表
     *
     * @param type   类型，如果是0则是商品收藏，如果是1则是专题收藏
     * @param page   分页页数
     * @param limit   分页大小
     * @return 用户收藏列表
     */
    @GetMapping("list")
    public Object list(@ApiIgnore @CurrentMember MemberInfo currentMember,
                       @NotNull Integer type,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "add_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {

//        List<LitemallCollect> collectList = collectService.queryByType(currentMember.getId(), type, page, limit, sort, order);
        List<LitemallCollect> collectList = collectService.list(new LambdaQueryWrapper<LitemallCollect>()
            .eq(LitemallCollect::getUserId,currentMember.getId())
            .eq(null != type,LitemallCollect::getType,type)
            .orderByDesc(LitemallCollect::getAddTime)
        );
        List<Object> collects = new ArrayList<>(collectList.size());
        for (LitemallCollect collect : collectList) {
            Map<String, Object> c = new HashMap<String, Object>();
            c.put("id", collect.getId());
            c.put("type", collect.getType());
            c.put("valueId", collect.getValueId());
            if (type == 0){
            	//查询商品信息
                LitemallGoods goods = goodsService.getById(collect.getValueId());
                c.put("name", goods.getName());
                c.put("brief", goods.getBrief());
                c.put("picUrl", goods.getPicUrl());
                c.put("retailPrice", goods.getRetailPrice());
            } else {
            	//查询专题信息
                LitemallTopic topic = topicService.getById(collect.getValueId());
                c.put("title", topic.getTitle());
                c.put("subtitle", topic.getTitle());
                c.put("price", topic.getPrice());
                c.put("picUrl", topic.getPicUrl());
            }
            collects.add(c);
        }

        return ResponseUtil.okList(collects, collectList);
    }

    /**
     * 用户收藏添加或删除
     * <p>
     * 如果商品没有收藏，则添加收藏；如果商品已经收藏，则删除收藏状态。
     *
     * @param body   请求内容，{ type: xxx, valueId: xxx }
     * @return 操作结果
     */
    @PostMapping("addordelete")
    public Object addordelete(@ApiIgnore @CurrentMember MemberInfo currentMember,
                              @RequestBody String body) {
        Integer type = JacksonUtil.parseInteger(body, "type");
        Integer valueId = JacksonUtil.parseInteger(body, "valueId");
        if (!ObjectUtils.allNotNull(type, valueId)) {
            return ResponseUtil.badArgument();
        }

        LitemallCollect collect = collectService.getOne(new LambdaQueryWrapper<LitemallCollect>()
                .eq(LitemallCollect::getId,valueId)
                .eq(LitemallCollect::getUserId,currentMember.getId())
                .eq(LitemallCollect::getType,type)
                ,false);

        if (collect != null) {
            collectService.removeById(collect.getId());
        } else {
            collect = new LitemallCollect();
            collect.setUserId(currentMember.getId());
            collect.setValueId(Long.valueOf(valueId));
            collect.setType(type);
            collectService.save(collect);
        }
        return ResponseUtil.ok();
    }
}
