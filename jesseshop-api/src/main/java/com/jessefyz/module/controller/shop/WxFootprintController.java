package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.common.utils.JacksonUtil;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.domain.LitemallFootprint;
import com.jessefyz.module.shop.domain.LitemallGoods;
import com.jessefyz.module.shop.service.ILitemallFootprintService;
import com.jessefyz.module.shop.service.ILitemallGoodsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户访问足迹服务
 */
@RestController
@RequestMapping("/wx/footprint")
@Validated
public class WxFootprintController extends BaseController {
    private final Log logger = LogFactory.getLog(WxFootprintController.class);

    @Autowired
    private ILitemallFootprintService footprintService;
    @Autowired
    private ILitemallGoodsService goodsService;

    /**
     * 删除用户足迹
     *
     * @param body   请求内容， { id: xxx }
     * @return 删除操作结果
     */
    @PostMapping("delete")
    public Object delete(@ApiIgnore @CurrentMember MemberInfo currentMember,
                         @RequestBody String body) {
        if (body == null) {
            return ResponseUtil.badArgument();
        }

        Integer footprintId = JacksonUtil.parseInteger(body, "id");
        if (footprintId == null) {
            return ResponseUtil.badArgument();
        }
        LitemallFootprint footprint = footprintService.getOne(new LambdaQueryWrapper<LitemallFootprint>()
            .eq(LitemallFootprint::getId,footprintId)
            .eq(LitemallFootprint::getUserId,currentMember.getId())
        );
        if (footprint == null) {
            return ResponseUtil.badArgumentValue();
        }
        footprintService.removeById(footprint.getId());
        return ResponseUtil.ok();
    }

    /**
     * 用户足迹列表
     *
     * @return 用户足迹列表
     */
    @GetMapping("list")
    public Object list(@ApiIgnore @CurrentMember MemberInfo currentMember,
                       @RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "10") Integer pageSize) {
       startPage();
        List<LitemallFootprint> footprintList = footprintService.list(new LambdaQueryWrapper<LitemallFootprint>()
            .eq(LitemallFootprint::getUserId,currentMember.getId())
            .orderByDesc()
        );
        List<Object> footprintVoList = new ArrayList<>(footprintList.size());
        for (LitemallFootprint footprint : footprintList) {
            Map<String, Object> c = new HashMap<String, Object>();
            c.put("id", footprint.getId());
            c.put("goodsId", footprint.getGoodsId());
            c.put("addTime", DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS,footprint.getAddTime()));
            LitemallGoods goods = goodsService.getById(footprint.getGoodsId());
            c.put("name", goods.getName());
            c.put("brief", goods.getBrief());
            c.put("picUrl", goods.getPicUrl());
            c.put("retailPrice", goods.getRetailPrice());
            footprintVoList.add(c);
        }

        return ResponseUtil.okList(footprintVoList, footprintList);
    }

}
