package com.jessefyz.module.controller.shop;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.GrouponConstant;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.member.service.IMemberInfoService;
import com.jessefyz.module.shop.constants.SystemConfig;
import com.jessefyz.module.shop.domain.*;
import com.jessefyz.module.shop.service.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 商品服务
 */
@RestController
@RequestMapping("/wx/goods")
@Validated
public class WxGoodsController {
	private final Log logger = LogFactory.getLog(WxGoodsController.class);

	@Autowired
	private ILitemallGoodsService goodsService;

	@Autowired
	private ILitemallGoodsProductService productService;

	@Autowired
	private ILitemallIssueService goodsIssueService;

	@Autowired
	private ILitemallGoodsAttributeService goodsAttributeService;

	@Autowired
	private ILitemallBrandService brandService;

	@Autowired
	private ILitemallCommentService commentService;

	@Autowired
	private IMemberInfoService memberInfoService;

	@Autowired
	private ILitemallCollectService collectService;

	@Autowired
	private ILitemallFootprintService footprintService;

	@Autowired
	private ILitemallCategoryService categoryService;

	@Autowired
	private ILitemallSearchHistoryService searchHistoryService;

	@Autowired
	private ILitemallGoodsSpecificationService goodsSpecificationService;

	@Autowired
	private ILitemallGrouponRulesService rulesService;

	private final static ArrayBlockingQueue<Runnable> WORK_QUEUE = new ArrayBlockingQueue<>(9);

	private final static RejectedExecutionHandler HANDLER = new ThreadPoolExecutor.CallerRunsPolicy();

	private static ThreadPoolExecutor executorService = new ThreadPoolExecutor(16, 16, 1000, TimeUnit.MILLISECONDS, WORK_QUEUE, HANDLER);

	/**
	 * 商品详情
	 * <p>
	 * 用户可以不登录。
	 * 如果用户登录，则记录用户足迹以及返回用户收藏信息。
	 *
	 * @param currentMember 用户
	 * @param id     商品ID
	 * @return 商品详情
	 */
	@GetMapping("detail")
	public Object detail(@ApiIgnore @CurrentMember MemberInfo currentMember,
						 @NotNull Long id) {
		// 商品信息
		LitemallGoods info = goodsService.getById(id);

		// 商品属性
		Callable<List> goodsAttributeListCallable = () -> goodsAttributeService.queryByGid(id);

		// 商品规格 返回的是定制的GoodsSpecificationVo
		Callable<Object> objectCallable = () -> goodsSpecificationService.getSpecificationVoList(id);

		// 商品规格对应的数量和价格
		Callable<List> productListCallable = () -> productService.queryByGid(id);

		// 商品问题，这里是一些通用问题
		Callable<List> issueCallable = () -> goodsIssueService.querySelective("", 1, 4, "", "");

		// 商品品牌商
		Callable<LitemallBrand> brandCallable = ()->{
			Long brandId = info.getBrandId();
			LitemallBrand brand;
			if (brandId == 0) {
				brand = new LitemallBrand();
			} else {
				brand = brandService.getById(info.getBrandId());
			}
			return brand;
		};

		// 评论
		Callable<Map> commentsCallable = () -> {
			List<LitemallComment> comments = commentService.list(new LambdaQueryWrapper<LitemallComment>()
				.eq(LitemallComment::getValueId,id)
					.orderByDesc(LitemallComment::getAddTime)
				.last("limit 2")
			);
			List<Map<String, Object>> commentsVo = new ArrayList<>(comments.size());
			long commentCount = PageInfo.of(comments).getTotal();
			for (LitemallComment comment : comments) {
				Map<String, Object> c = new HashMap<>();
				c.put("id", comment.getId());
				c.put("addTime", comment.getAddTime());
				c.put("content", comment.getContent());
				c.put("adminContent", comment.getAdminContent());
				MemberInfo memberInfo = memberInfoService.getById(comment.getUserId());
				c.put("nickname", memberInfo == null ? "" : memberInfo.getNickname());
				c.put("avatar", memberInfo == null ? "" : memberInfo.getPic());
				c.put("picList", comment.getPicUrls());
				commentsVo.add(c);
			}
			Map<String, Object> commentList = new HashMap<>();
			commentList.put("count", commentCount);
			commentList.put("data", commentsVo);
			return commentList;
		};

		//团购信息
		Callable<List> grouponRulesCallable = () ->rulesService.list(new LambdaQueryWrapper<LitemallGrouponRules>()
			.eq(LitemallGrouponRules::getGoodsId,id)
			.eq(LitemallGrouponRules::getStatus, GrouponConstant.RULE_STATUS_ON)
		);

		// 用户收藏
		int userHasCollect = 0;
		if (currentMember != null) {
			userHasCollect = collectService.count(new LambdaQueryWrapper<LitemallCollect>()
				.eq(LitemallCollect::getUserId,currentMember.getId())
				.eq(LitemallCollect::getValueId,id)
			);
		}

		// 记录用户的足迹 异步处理
		if (currentMember != null) {
			executorService.execute(()->{
				LitemallFootprint footprint = new LitemallFootprint();
				footprint.setUserId(currentMember.getId());
				footprint.setGoodsId(id);
				footprintService.save(footprint);
			});
		}
		FutureTask<List> goodsAttributeListTask = new FutureTask<>(goodsAttributeListCallable);
		FutureTask<Object> objectCallableTask = new FutureTask<>(objectCallable);
		FutureTask<List> productListCallableTask = new FutureTask<>(productListCallable);
		FutureTask<List> issueCallableTask = new FutureTask<>(issueCallable);
		FutureTask<Map> commentsCallableTsk = new FutureTask<>(commentsCallable);
		FutureTask<LitemallBrand> brandCallableTask = new FutureTask<>(brandCallable);
        FutureTask<List> grouponRulesCallableTask = new FutureTask<>(grouponRulesCallable);

		executorService.submit(goodsAttributeListTask);
		executorService.submit(objectCallableTask);
		executorService.submit(productListCallableTask);
		executorService.submit(issueCallableTask);
		executorService.submit(commentsCallableTsk);
		executorService.submit(brandCallableTask);
		executorService.submit(grouponRulesCallableTask);

		Map<String, Object> data = new HashMap<>();

		try {
			data.put("info", info);
			data.put("userHasCollect", userHasCollect);
			data.put("issue", issueCallableTask.get());
			data.put("comment", commentsCallableTsk.get());
			data.put("specificationList", objectCallableTask.get());
			data.put("productList", productListCallableTask.get());
			data.put("attribute", goodsAttributeListTask.get());
			data.put("brand", brandCallableTask.get());
			data.put("groupon", grouponRulesCallableTask.get());
			//SystemConfig.isAutoCreateShareImage()
			data.put("share", SystemConfig.isAutoCreateShareImage());

		}
		catch (Exception e) {
			e.printStackTrace();
		}

		//商品分享图片地址
		data.put("shareImage", info.getShareUrl());
		return ResponseUtil.ok(data);
	}

	/**
	 * 商品分类类目
	 *
	 * @param id 分类类目ID
	 * @return 商品分类类目
	 */
	@GetMapping("category")
	public Object category(@NotNull Long id) {
		LitemallCategory cur = categoryService.getById(id);
		LitemallCategory parent = null;
		List<LitemallCategory> children = null;

		if (cur.getParentId() == 0L) {
			parent = cur;
			children = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
				.eq(LitemallCategory::getParentId,cur.getId())
				.orderByAsc(LitemallCategory::getSortOrder)
			);
			cur = CollectionUtil.isNotEmpty(children) ? children.get(0) : cur;
		} else {
			parent = categoryService.getById(cur.getParentId());
			children = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
					.eq(LitemallCategory::getParentId,cur.getParentId())
					.orderByAsc(LitemallCategory::getSortOrder)
			);
		}
		Map<String, Object> data = new HashMap<>();
		data.put("currentCategory", cur);
		data.put("parentCategory", parent);
		data.put("brotherCategory", children);
		return ResponseUtil.ok(data);
	}

	/**
	 * 根据条件搜素商品
	 * <p>
	 * 1. 这里的前五个参数都是可选的，甚至都是空
	 * 2. 用户是可选登录，如果登录，则记录用户的搜索关键字
	 *
	 * @param categoryId 分类类目ID，可选
	 * @param brandId    品牌商ID，可选
	 * @param keyword    关键字，可选
	 * @param isNew      是否新品，可选
	 * @param isHot      是否热买，可选
	 * @param sort       排序方式，支持"add_time", "retail_price"或"name"
	 * @param order      排序类型，顺序或者降序
	 * @return 根据条件搜素的商品详情
	 */
	@GetMapping("list")
	public Object list(
		Integer categoryId,
		Integer brandId,
		String keyword,
		Integer isNew,
		Integer isHot,
		@ApiIgnore @CurrentMember MemberInfo currentMember,
		@RequestParam(defaultValue = "1") Integer pageNum,
		@RequestParam(defaultValue = "10") Integer pageSize,
		@RequestParam(defaultValue = "add_time") String sort,
		 @RequestParam(defaultValue = "desc") String order) {

		//添加到搜索历史
		if (currentMember != null && !StringUtils.isEmpty(keyword)) {
			LitemallSearchHistory searchHistoryVo = new LitemallSearchHistory();
			searchHistoryVo.setKeyword(keyword);
			searchHistoryVo.setUserId(currentMember.getId());
			searchHistoryVo.setFromType("wx");
			searchHistoryService.save(searchHistoryVo);
		}

		//查询列表数据
		List<LitemallGoods> goodsList = goodsService.querySelective(categoryId, brandId, keyword, isHot, isNew, pageNum, pageSize, sort, order);

		// 查询商品所属类目列表。
		List<Long> goodsCatIds = goodsService.getByCatIds(brandId, keyword, isHot, isNew);
		List<LitemallCategory> categoryList = null;
		if (goodsCatIds.size() != 0) {
			categoryList = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
				.in(LitemallCategory::getId,goodsCatIds)
				.eq(LitemallCategory::getLevel,"L2")
			);
		} else {
			categoryList = new ArrayList<>(0);
		}

		PageInfo<LitemallGoods> pagedList = PageInfo.of(goodsList);

		Map<String, Object> entity = new HashMap<>();
		entity.put("list", goodsList);
		entity.put("total", pagedList.getTotal());
		entity.put("page", pagedList.getPageNum());
		entity.put("limit", pagedList.getPageSize());
		entity.put("pages", pagedList.getPages());
		entity.put("filterCategoryList", categoryList);
		// 因为这里需要返回额外的filterCategoryList参数，因此不能方便使用ResponseUtil.okList
		return ResponseUtil.ok(entity);
	}

	/**
	 * 商品详情页面“大家都在看”推荐商品
	 *
	 * @param id, 商品ID
	 * @return 商品详情页面推荐商品
	 */
	@GetMapping("related")
	public Object related(@NotNull Long id) {
		LitemallGoods goods = goodsService.getById(id);
		if (goods == null) {
			return ResponseUtil.badArgumentValue();
		}
		// 目前的商品推荐算法仅仅是推荐同类目的其他商品
		Long cid = goods.getCategoryId();
		// 查找六个相关商品
		List<LitemallGoods> goodsList = goodsService.list(new LambdaQueryWrapper<LitemallGoods>()
			.eq(LitemallGoods::getCategoryId,cid)
			.orderByDesc(LitemallGoods::getAddTime)
			.last("limit 6")
		);
		return ResponseUtil.okList(goodsList);
	}

	/**
	 * 在售的商品总数
	 *
	 * @return 在售的商品总数
	 */
	@GetMapping("count")
	public Object count() {
		Integer goodsCount = goodsService.count(new LambdaQueryWrapper<LitemallGoods>()
			.eq(LitemallGoods::getIsOnSale,1)
		);
		return ResponseUtil.ok(goodsCount);
	}

}
