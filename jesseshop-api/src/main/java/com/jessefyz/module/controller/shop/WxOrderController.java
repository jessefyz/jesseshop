package com.jessefyz.module.controller.shop;

import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.service.WxOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/wx/order")
@Validated
@Api(tags = "订单信息")
public class WxOrderController {
    private final Log logger = LogFactory.getLog(WxOrderController.class);

    @Autowired
    private WxOrderService wxOrderService;

    /**
     * 订单列表
     *
     * @param showType 显示类型，如果是0则是全部订单
     * @param sort     排序字段
     * @param order     排序方式
     * @return 订单列表
     */
    @GetMapping("list")
    public Object list(@ApiIgnore @CurrentMember MemberInfo currentMember,
                       @RequestParam(defaultValue = "0") Integer showType,
                       @RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "add_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {
        return wxOrderService.list(currentMember.getId().intValue(), showType, pageNum, pageSize, sort, order);
    }

    /**
     * 订单详情
     *
     * @param orderId 订单ID
     * @return 订单详情
     */
    @GetMapping("detail")
    public Object detail(@ApiIgnore @CurrentMember MemberInfo currentMember, @NotNull Integer orderId) {
        return wxOrderService.detail(currentMember.getId().intValue(), orderId);
    }

    /**
     * 提交订单
     *
     * @param body   订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,  grouponLinkId: xxx}
     * @return 提交订单操作结果
     */
    @PostMapping("submit")
    public Object submit(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        return wxOrderService.submit(currentMember.getId().intValue(), body);
    }

    /**
     * 取消订单
     *
     * @param body   订单信息，{ orderId：xxx }
     * @return 取消订单操作结果
     */
    @PostMapping("cancel")
    public Object cancel(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        return wxOrderService.cancel(currentMember.getId().intValue(), body);
    }

    /**
     * 付款订单的预支付会话标识
     *
     * @param body   订单信息，{ orderId：xxx }
     * @return 支付订单ID
     */
    @PostMapping("prepay")
    public Object prepay(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body, HttpServletRequest request) {
        return wxOrderService.prepay(currentMember.getId().intValue(), body, request);
    }

    /**
     * 微信H5支付
     * @param body
     * @param request
     * @return
     */
    @PostMapping("h5pay")
    public Object h5pay(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body, HttpServletRequest request) {
        return wxOrderService.h5pay(currentMember.getId().intValue(), body, request);
    }

    /**
     * 微信付款成功或失败回调接口
     * <p>
     *  TODO
     *  注意，这里pay-notify是示例地址，建议开发者应该设立一个隐蔽的回调地址
     *
     * @param request 请求内容
     * @param response 响应内容
     * @return 操作结果
     */
    @ApiOperation("支付回调")
    @PostMapping("pay-notify")
    public Object payNotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.payNotify(request, response);
    }

    /**
     * 订单申请退款
     *
     * @param currentMember 用户ID
     * @param body   订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @PostMapping("refund")
    public Object refund(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        return wxOrderService.refund(currentMember.getId().intValue(), body);
    }

    /**
     * 确认收货
     *
     * @param currentMember 用户ID
     * @param body   订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("confirm")
    public Object confirm(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        return wxOrderService.confirm(currentMember.getId().intValue(), body);
    }

    /**
     * 删除订单
     *
     * @param currentMember 用户ID
     * @param body   订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("delete")
    public Object delete(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        return wxOrderService.delete(currentMember.getId().intValue(), body);
    }

    /**
     * 待评价订单商品信息
     *
     * @param currentMember  用户ID
     * @param orderId 订单ID
     * @param goodsId 商品ID
     * @return 待评价订单商品信息
     */
    @GetMapping("goods")
    public Object goods(@ApiIgnore @CurrentMember MemberInfo currentMember,
                        @NotNull Integer orderId,
                        @NotNull Integer goodsId) {
        return wxOrderService.goods(currentMember.getId().intValue(), orderId, goodsId);
    }

    /**
     * 评价订单商品
     *
     * @param currentMember 用户ID
     * @param body   订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("comment")
    public Object comment(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        return wxOrderService.comment(currentMember.getId().intValue(), body);
    }

}
