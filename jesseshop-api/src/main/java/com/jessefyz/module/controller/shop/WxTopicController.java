package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.domain.LitemallCollect;
import com.jessefyz.module.shop.domain.LitemallGoods;
import com.jessefyz.module.shop.domain.LitemallTopic;
import com.jessefyz.module.shop.service.ILitemallCollectService;
import com.jessefyz.module.shop.service.ILitemallGoodsService;
import com.jessefyz.module.shop.service.ILitemallTopicService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 专题服务
 */
@RestController
@RequestMapping("/wx/topic")
@Validated
public class WxTopicController {
    private final Log logger = LogFactory.getLog(WxTopicController.class);

    @Autowired
    private ILitemallTopicService topicService;
    @Autowired
    private ILitemallGoodsService goodsService;
	@Autowired
	private ILitemallCollectService collectService;

    /**
     * 专题列表
     *
     * @param pageNum 分页页数
     * @param pageSize 分页大小
     * @return 专题列表
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "10") Integer pageSize,
                       @RequestParam(defaultValue = "add_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {
        List<LitemallTopic> topicList = topicService.queryList(pageNum, pageSize, sort, order);
        return ResponseUtil.okList(topicList);
    }

    /**
     * 专题详情
     *
     * @param id 专题ID
     * @return 专题详情
     */
    @GetMapping("detail")
    public Object detail(@ApiIgnore @CurrentMember MemberInfo currentMember, @NotNull Integer id) {
        LitemallTopic topic = topicService.getById(id);
        List<LitemallGoods> goods = new ArrayList<>();
        for (Integer i : topic.getGoods()) {
            LitemallGoods good = goodsService.getOne(new LambdaQueryWrapper<LitemallGoods>()
                    .eq(LitemallGoods::getId,i)
                    .eq(LitemallGoods::getIsOnSale,1)
                    ,false);
            if (null != good) {
                goods.add(good);
            }
        }

		// 用户收藏
		int userHasCollect = 0;
		if (currentMember != null) {
			userHasCollect = collectService.count(new LambdaQueryWrapper<LitemallCollect>()
                .eq(LitemallCollect::getUserId,currentMember.getId())
                .eq(LitemallCollect::getType,1)
                .eq(LitemallCollect::getValueId,id)
            );
		}

        Map<String, Object> entity = new HashMap<String, Object>();
        entity.put("topic", topic);
        entity.put("goods", goods);
        entity.put("userHasCollect", userHasCollect);
        return ResponseUtil.ok(entity);
    }

    /**
     * 相关专题
     *
     * @param id 专题ID
     * @return 相关专题
     */
    @GetMapping("related")
    public Object related(@NotNull Integer id) {
        List<LitemallTopic> topicRelatedList = topicService.queryRelatedList(id, 0, 4);
        return ResponseUtil.okList(topicRelatedList);
    }
}
