package com.jessefyz.module.controller.shop;

import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.service.ILitemallOrderService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户服务
 */
@RestController
@RequestMapping("/wx/user")
@Validated
public class WxUserController {
    private final Log logger = LogFactory.getLog(WxUserController.class);

    @Autowired
    private ILitemallOrderService orderService;

    /**
     * 用户个人页面数据
     * <p>
     * 目前是用户订单统计信息
     *
     * @return 用户个人页面数据
     */
    @GetMapping("index")
    public Object list(@ApiIgnore @CurrentMember MemberInfo currentMember) {

        Map<Object, Object> data = new HashMap<Object, Object>();
        data.put("order", orderService.orderInfo(currentMember.getId()));
        return ResponseUtil.ok(data);
    }

}
