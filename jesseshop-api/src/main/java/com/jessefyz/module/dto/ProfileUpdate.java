package com.jessefyz.module.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Data
@Validated
@ApiModel
public class ProfileUpdate {

    @ApiModelProperty(value = "密码",required = true)
    private String avatar;

    @ApiModelProperty(value = "性别",required = true)
    private Integer gender;

    @ApiModelProperty(value = "昵称",required = true)
    @NotBlank(message = "昵称不能为空")
    private String nickname;

}
