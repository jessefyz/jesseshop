package com.jessefyz.module.dto;


import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Validated
public class WxLoginInfo {

    @NotBlank(message = "code不能为空")
    private String code;

    @NotNull(message = "userInfo不能为空")
    private UserInfo userInfo;

}
