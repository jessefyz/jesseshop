package com.jessefyz.module.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.constants.CouponConstant;
import com.jessefyz.module.shop.domain.LitemallCart;
import com.jessefyz.module.shop.domain.LitemallCoupon;
import com.jessefyz.module.shop.domain.LitemallCouponUser;
import com.jessefyz.module.shop.service.ILitemallCouponService;
import com.jessefyz.module.shop.service.ILitemallCouponUserService;
import com.jessefyz.module.shop.service.ILitemallGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class CouponVerifyService {

    @Autowired
    private ILitemallCouponUserService couponUserService;
    @Autowired
    private ILitemallCouponService couponService;
    @Autowired
    private ILitemallGoodsService goodsService;

    /**
     * 检测优惠券是否适合
     *
     * @param userId
     * @param couponId
     * @param checkedGoodsPrice
     * @return
     */
    public LitemallCoupon checkCoupon(Long userId, Long couponId, Long userCouponId, BigDecimal checkedGoodsPrice, List<LitemallCart> cartList) {
        LitemallCoupon coupon = couponService.getById(couponId);
        if (null == coupon) {
            return null;
        }

        LitemallCouponUser couponUser = couponUserService.getById(userCouponId);
        if (couponUser == null) {
            couponUser = couponUserService.getOne(new LambdaQueryWrapper<LitemallCouponUser>()
                .eq(LitemallCouponUser::getUserId,userId)
                .eq(LitemallCouponUser::getCouponId,couponId)
                .orderByDesc(BaseShopEntity::getAddTime)
                .last("limit 1")
            ,false);
        } else if (!couponId.equals(couponUser.getCouponId())) {
            return null;
        }

        if (couponUser == null) {
            return null;
        }

        // 检查是否超期
        Integer timeType = coupon.getTimeType();
        Integer days = coupon.getDays();
        LocalDateTime now = LocalDateTime.now();
        long currentTimeMillis = System.currentTimeMillis();
        if (timeType.equals(CouponConstant.TIME_TYPE_TIME)) {
            if (currentTimeMillis < coupon.getStartTime().getTime() ||
                currentTimeMillis > coupon.getEndTime().getTime()
            ) {
                return null;
            }
        }
        else if(timeType.equals(CouponConstant.TIME_TYPE_DAYS)) {
            long expired = DateUtils.addDays(couponUser.getAddTime(),days).getTime();
            if (currentTimeMillis > expired) {
                return null;
            }
        }
        else {
            return null;
        }

        // 检测商品是否符合
        Map<Long, List<LitemallCart>> cartMap = new HashMap<>();
        //可使用优惠券的商品或分类
        List<Integer> goodsValueList = new ArrayList<>();
        if (null != coupon.getGoodsValue()) {
            goodsValueList= new ArrayList<>(Arrays.asList(coupon.getGoodsValue()));
        }
        Integer goodType = coupon.getGoodsType();

        if (goodType.equals(CouponConstant.GOODS_TYPE_CATEGORY) ||
                goodType.equals((CouponConstant.GOODS_TYPE_ARRAY))) {
            for (LitemallCart cart : cartList) {
                Long key = goodType.equals(CouponConstant.GOODS_TYPE_ARRAY) ? cart.getGoodsId() :
                        goodsService.getById(cart.getGoodsId()).getCategoryId();
                List<LitemallCart> carts = cartMap.get(key);
                if (carts == null) {
                    carts = new LinkedList<>();
                }
                carts.add(cart);
                cartMap.put(key, carts);
            }
            //购物车中可以使用优惠券的商品或分类
            goodsValueList.retainAll(cartMap.keySet());
            //可使用优惠券的商品的总价格
            BigDecimal total = new BigDecimal(0);

            for (Integer goodsId : goodsValueList) {
                List<LitemallCart> carts = cartMap.get(goodsId);
                for (LitemallCart cart : carts) {
                    total = total.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
                }
            }
            //是否达到优惠券满减金额
            if (total.compareTo(coupon.getMin()) == -1) {
                return null;
            }
        }

        // 检测订单状态
        Integer status = coupon.getStatus();
        if (!status.equals(CouponConstant.STATUS_NORMAL)) {
            return null;
        }
        // 检测是否满足最低消费
        if (checkedGoodsPrice.compareTo(coupon.getMin()) == -1) {
            return null;
        }
        return coupon;
    }

}
