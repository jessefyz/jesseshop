package com.jessefyz.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.module.dto.*;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.req.MiniAppLoginReq;
import com.jessefyz.module.vo.MemberVo;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录service
 *
 */
public interface IApiLoginService extends IService<MemberInfo> {

    AjaxResult<MemberVo> loginBywechatMiniApp(WxLoginInfo wxLoginInfo, HttpServletRequest request)  throws Exception ;

    MemberVo getMemberVo(MemberInfo memberInfo);

    AjaxResult<MemberVo> loginByAccount(AccountLoginInfo accountLoginInfo, HttpServletRequest request);

    void registerCaptcha(RegisterCaptcha registerCaptcha);

    AjaxResult<MemberVo> register(Register register, HttpServletRequest request);

    void captcha(Captcha captcha);

    void reset(ResetPassword resetPassword);

    void resetPhone(MemberInfo currentMember, ResetPassword resetPassword);

    void profile(MemberInfo currentMember, ProfileUpdate profileUpdate);

    void bindPhone(MemberInfo currentMember, WxBindPhone wxBindPhone);
}
