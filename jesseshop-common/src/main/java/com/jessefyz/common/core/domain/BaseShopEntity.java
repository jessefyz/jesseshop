package com.jessefyz.common.core.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jessefyz.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 商城功能实体的基类
 *
 * @author jessefyz
 */
@Data
public class BaseShopEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 逻辑删除 */
//    @JsonIgnore
    @TableLogic
    @Excel(name = "逻辑删除")
    private Integer deleted;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "add_time" , fill = FieldFill.INSERT)
    private Date addTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time" , fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /** 请求参数 */
    @JsonIgnore
    @TableField(exist = false)
    private Map<String, Object> params;


}
