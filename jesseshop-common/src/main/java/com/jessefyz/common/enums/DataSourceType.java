package com.jessefyz.common.enums;

/**
 * 数据源
 * 
 * @author jessefyz
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
