package com.jessefyz.module.member.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.module.member.mapper.MemberInfoMapper;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.member.service.IMemberInfoService;

/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName MemberInfo
 * @Author jessefyz
 * @Date 2020-11-06
 * @Version 1.0.0
 * @Description 会员信息Service业务层处理
 */
@Service
public class MemberInfoServiceImpl extends ServiceImpl<MemberInfoMapper, MemberInfo> implements IMemberInfoService {

}
