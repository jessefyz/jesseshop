package com.jessefyz.module.merchant.domain;

/**
 * @Author:hanguanglei
 * @Description:
 * @Date: Created in 2020-03-26 14:58
 * @Modified By:
 */

public class UploadMedia {

    private String media_id;

    private String url;

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
