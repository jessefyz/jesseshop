package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 收藏对象 litemall_collect
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallCollect extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户表的用户ID */
    @Excel(name = "用户表的用户ID")
    private Long userId;

    /** 如果type=0，则是商品ID；如果type=1，则是专题ID */
    @Excel(name = "如果type=0，则是商品ID；如果type=1，则是专题ID")
    private Long valueId;

    /** 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID */
    @Excel(name = "收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID")
    private Integer type;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("valueId", getValueId())
            .append("type", getType())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
