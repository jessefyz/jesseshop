package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 评论对象 litemall_comment
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@TableName(value = "litemall_comment",autoResultMap = true)
public class LitemallComment extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 如果type=0，则是商品评论；如果是type=1，则是专题评论。 */
    @Excel(name = "如果type=0，则是商品评论；如果是type=1，则是专题评论。")
    private Long valueId;

    /** 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；如果type=3，则是订单商品评论。 */
    @Excel(name = "评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；如果type=3，则是订单商品评论。")
    private Integer type;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String content;

    /** 评论内容 */
    @Excel(name = "管理员回复内容")
    private String adminContent;

    /** 用户表的用户ID */
    @Excel(name = "用户表的用户ID")
    private Long userId;

    /** 是否含有图片 */
    @Excel(name = "是否含有图片")
    private Integer hasPicture;

    /** 图片地址列表，采用JSON数组格式 */
    @Excel(name = "图片地址列表，采用JSON数组格式")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] picUrls;

    /** 评分， 1-5 */
    @Excel(name = "评分， 1-5")
    private Integer star;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("valueId", getValueId())
            .append("type", getType())
            .append("content", getContent())
            .append("userId", getUserId())
            .append("hasPicture", getHasPicture())
            .append("picUrls", getPicUrls())
            .append("star", getStar())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
