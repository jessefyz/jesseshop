package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 意见反馈对象 litemall_feedback
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallFeedback extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户表的用户ID */
    @Excel(name = "用户表的用户ID")
    private Long userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String username;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 反馈类型 */
    @Excel(name = "反馈类型")
    private String feedType;

    /** 反馈内容 */
    @Excel(name = "反馈内容")
    private String content;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 是否含有图片 */
    @Excel(name = "是否含有图片")
    private Integer hasPicture;

    /** 图片地址列表，采用JSON数组格式 */
    @Excel(name = "图片地址列表，采用JSON数组格式")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] picUrls;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("username", getUsername())
            .append("mobile", getMobile())
            .append("feedType", getFeedType())
            .append("content", getContent())
            .append("status", getStatus())
            .append("hasPicture", getHasPicture())
            .append("picUrls", getPicUrls())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
