package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 关键字对象 litemall_keyword
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallKeyword extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 关键字 */
    @Excel(name = "关键字")
    private String keyword;

    /** 关键字的跳转链接 */
    @Excel(name = "关键字的跳转链接")
    private String url;

    /** 是否是热门关键字 */
    @Excel(name = "是否是热门关键字")
    private Integer isHot;

    /** 是否是默认关键字 */
    @Excel(name = "是否是默认关键字")
    private Integer isDefault;

    /** 排序 */
    @Excel(name = "排序")
    private Long sortOrder;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("keyword", getKeyword())
            .append("url", getUrl())
            .append("isHot", getIsHot())
            .append("isDefault", getIsDefault())
            .append("sortOrder", getSortOrder())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
