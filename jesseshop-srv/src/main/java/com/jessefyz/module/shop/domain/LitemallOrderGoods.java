package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单商品对象 litemall_order_goods
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(autoResultMap = true)
public class LitemallOrderGoods extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单表的订单ID */
    @Excel(name = "订单表的订单ID")
    private Long orderId;

    /** 商品表的商品ID */
    @Excel(name = "商品表的商品ID")
    private Long goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsSn;

    /** 商品货品表的货品ID */
    @Excel(name = "商品货品表的货品ID")
    private Long productId;

    /** 商品货品的购买数量 */
    @Excel(name = "商品货品的购买数量")
    private Integer number;

    /** 商品货品的售价 */
    @Excel(name = "商品货品的售价")
    private BigDecimal price;

    /** 商品货品的规格列表 */
    @Excel(name = "商品货品的规格列表")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] specifications;

    /** 商品货品图片或者商品图片 */
    @Excel(name = "商品货品图片或者商品图片")
    private String picUrl;

    /** 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果其他值，则是comment表里面的评论ID。 */
    @Excel(name = "订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果其他值，则是comment表里面的评论ID。")
    private Long comment;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsSn", getGoodsSn())
            .append("productId", getProductId())
            .append("number", getNumber())
            .append("price", getPrice())
            .append("specifications", getSpecifications())
            .append("picUrl", getPicUrl())
            .append("comment", getComment())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
