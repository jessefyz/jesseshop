package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 行政区域对象 litemall_region
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallRegion
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0 */
    @Excel(name = "行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0")
    private Long pid;


    private Long parentId;
    /** 行政区域名称 */
    @Excel(name = "行政区域名称")
    private String name;

    /** 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县 */
    @Excel(name = "行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县")
    private Integer type;

    /** 行政区域编码 */
    @Excel(name = "行政区域编码")
    private Long code;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pid", getPid())
            .append("name", getName())
            .append("type", getType())
            .append("code", getCode())
            .toString();
    }
}
