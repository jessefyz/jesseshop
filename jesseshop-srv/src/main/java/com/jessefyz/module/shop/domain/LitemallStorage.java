package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 文件存储对象 litemall_storage
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallStorage extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 文件的唯一索引 */
    @Excel(name = "文件的唯一索引")
    private String key;

    /** 文件名 */
    @Excel(name = "文件名")
    private String name;

    /** 文件类型 */
    @Excel(name = "文件类型")
    private String type;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private Long size;

    /** 文件访问链接 */
    @Excel(name = "文件访问链接")
    private String url;

    private String mediaId;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("key", getKey())
            .append("name", getName())
            .append("type", getType())
            .append("size", getSize())
            .append("url", getUrl())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
