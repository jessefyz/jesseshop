package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonIntegerArrayTypeHandler;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 专题对象 litemall_topic
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@TableName(autoResultMap = true)
public class LitemallTopic extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 专题标题 */
    @Excel(name = "专题标题")
    private String title;

    /** 专题子标题 */
    @Excel(name = "专题子标题")
    private String subtitle;

    /** 专题内容，富文本格式 */
    @Excel(name = "专题内容，富文本格式")
    private String content;

    /** 专题相关商品最低价 */
    @Excel(name = "专题相关商品最低价")
    private Double price;

    /** 专题阅读量 */
    @Excel(name = "专题阅读量")
    private String readCount;

    /** 专题图片 */
    @Excel(name = "专题图片")
    private String picUrl;

    /** 排序 */
    @Excel(name = "排序")
    private Long sortOrder;

    /** 专题相关商品，采用JSON数组格式 */
    @Excel(name = "专题相关商品，采用JSON数组格式")
    @TableField(typeHandler = JsonIntegerArrayTypeHandler.class)
    private Integer[] goods;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subtitle", getSubtitle())
            .append("content", getContent())
            .append("price", getPrice())
            .append("readCount", getReadCount())
            .append("picUrl", getPicUrl())
            .append("sortOrder", getSortOrder())
            .append("goods", getGoods())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
