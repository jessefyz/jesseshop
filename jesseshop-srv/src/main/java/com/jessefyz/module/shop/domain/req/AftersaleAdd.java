package com.jessefyz.module.shop.domain.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName AftersaleAdd
 * @Author jessefyz
 * @Date 2021-09-08
 * @Version 1.0.0
 * @Description 售后Add对象
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
public class AftersaleAdd {
    private static final long serialVersionUID = 1L;

    /** 售后编号 */
    @ApiModelProperty("售后编号")
    private String aftersaleSn;

    /** 订单ID */
    @ApiModelProperty(value = "订单ID",required = true)
    @NotNull(message = "请输入订单ID")
    private Long orderId;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID",required = true)
    @NotNull(message = "请输入用户ID")
    private Long userId;

    /** 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款 */
    @ApiModelProperty("售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款")
    @NotNull(message = "请选择退货类型")
    private Integer type;

    /** 退款原因 */
    @ApiModelProperty("退款原因")
    @NotBlank(message = "请输入退款原因")
    private String reason;

    /** 退款金额 */
    @ApiModelProperty("退款金额")
    @NotNull(message = "请输入退款金额")
    private BigDecimal amount;

    /** 退款凭证图片链接数组 */
    @ApiModelProperty("退款凭证图片链接数组")
    private String pictures;



    /** 退款说明 */
    @ApiModelProperty(value = "退款说明",hidden = true)
    private String comment;

    /** 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消 */
    @ApiModelProperty("售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消")
    private Integer status;

    /** 管理员操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "管理员操作时间",hidden = true)
    private Date handleTime;

    /** 添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("添加时间")
    private Date addTime;

    /** 售后编号 */
    @ApiModelProperty("售后编号")
    private Integer deleted;

}
