package com.jessefyz.module.shop.domain.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.*;
import org.springframework.validation.annotation.Validated;

/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName AftersaleUpdate
 * @Author jessefyz
 * @Date 2021-09-08
 * @Version 1.0.0
 * @Description 售后更新请求体对象
 */
@Data
@ToString
@Validated
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
public class AftersaleUpdate extends AftersaleAdd {

    @NotNull(message = "ID不能为空")
    @ApiModelProperty(value = "ID",required = true)
    private Long id;
}
