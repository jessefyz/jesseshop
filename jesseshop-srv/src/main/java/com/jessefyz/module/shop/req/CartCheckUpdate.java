package com.jessefyz.module.shop.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 购物车商品货品勾选状态 更新请求体
 *
 * @author Jesse
 * @date 2021-09-03
 */
@Data
@ApiModel
@Validated
public class CartCheckUpdate
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "货品ID列表",required = true)
    @NotEmpty(message = "货品ID不能为空")
    private List<Integer> productIds;

    /** 订单费用， = goods_price + freight_price - coupon_price*/
    @ApiModelProperty(value = "是否勾选 1是 0否",required = true)
    @NotNull(message = "请选择是否勾选")
    private Integer isChecked;

}
