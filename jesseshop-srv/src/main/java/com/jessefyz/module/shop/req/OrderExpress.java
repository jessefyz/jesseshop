package com.jessefyz.module.shop.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 订单发货 前端提交对象
 *
 * @author Jesse
 * @date 2021-09-03
 */
@Data
@ApiModel
public class OrderExpress
{
    private static final long serialVersionUID = 1L;

    /** 发货编号 */
    @ApiModelProperty(value = "发货编号",required = true)
    @NotBlank(message = "请输入发货编号")
    private String shipSn;

    /** 发货快递公司 */
    @ApiModelProperty(value = "发货快递公司",required = true)
    @NotBlank(message = "请选择发货快递公司")
    private String shipChannel;

}
