package com.jessefyz.module.shop.req;

import com.jessefyz.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 订单收款 前端提交对象
 *
 * @author Jesse
 * @date 2021-09-03
 */
@Data
@ApiModel
public class OrderPay
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单id",required = true)
    @NotNull(message = "订单ID不能为空")
    private Long id;

    /** 订单费用， = goods_price + freight_price - coupon_price*/
    @ApiModelProperty(value = "订单金额",required = true)
    @NotNull(message = "请输入订单金额")
    private BigDecimal orderPrice;

    /** 实付费用， = order_price - integral_price */
    @ApiModelProperty(value = "实付费用",required = true)
    @NotNull(message = "请输入实付金额")
    private BigDecimal actualPrice;

}
