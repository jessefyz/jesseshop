package com.jessefyz.module.shop.req;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.module.shop.domain.LitemallOrderGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 订单发货 前端提交对象
 *
 * @author Jesse
 * @date 2021-09-03
 */
@Data
@ApiModel
public class OrderShip
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单id",required = true)
    @NotNull(message = "订单ID不能为空")
    private Long id;

    /** 发货编号 */
    @ApiModelProperty(value = "发货编号",required = true)
    @NotBlank(message = "请输入发货编号")
    private String shipSn;

    /** 发货快递公司 */
    @ApiModelProperty(value = "发货快递公司",required = true)
    @NotBlank(message = "请选择发货快递公司")
    private String shipChannel;

}
