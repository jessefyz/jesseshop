package com.jessefyz.module.shop.service;

import com.jessefyz.module.shop.domain.Aftersale;
import com.jessefyz.module.shop.domain.vo.AftersaleVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.util.List;
/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName Aftersale
 * @Author jessefyz
 * @Date 2021-09-08
 * @Version 1.0.0
 * @Description 售后Service接口
 */
public interface IAftersaleService extends IService<Aftersale> {

    /**
     * @Description 查询列表返回VO 用于返回给前端的列表接口
     * @Author jessefyz
     * @Date 2021-09-08
     * @Param [lqw]
     * @Return List<AftersaleVo>
     */
    List<AftersaleVo> listVo(QueryWrapper<Aftersale> lqw);

    /**
     * @Description 通过查询详情VO 用于返回给前端的列详情接口
     * @Author jessefyz
     * @Date 2021-09-08
     * @Param [id]
     * @Return AftersaleVo
     */
    AftersaleVo getVoById(Long id);

    String generateAftersaleSn(Long id);

    boolean saveAftersale(Aftersale aftersale);

    boolean cancel(Long currentMemberId, Long id);

    void deleteByOrderId(Integer userId, Integer orderId);
}
