package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallBrand;
import com.jessefyz.module.shop.domain.LitemallOrder;
import com.jessefyz.module.shop.mapper.LitemallBrandMapper;
import com.jessefyz.module.shop.mapper.LitemallOrderMapper;
import com.jessefyz.module.shop.service.ILitemallBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 品牌商Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallBrandServiceImpl  extends ServiceImpl<LitemallBrandMapper, LitemallBrand> implements ILitemallBrandService
{

    /**
     * 查询品牌商
     *
     * @param id 品牌商ID
     * @return 品牌商
     */
    @Override
    public LitemallBrand selectLitemallBrandById(Long id)
    {
        return baseMapper.selectLitemallBrandById(id);
    }

    /**
     * 查询品牌商列表
     *
     * @param litemallBrand 品牌商
     * @return 品牌商
     */
    @Override
    public List<LitemallBrand> selectLitemallBrandList(LitemallBrand litemallBrand)
    {
        return baseMapper.selectLitemallBrandList(litemallBrand);
    }

    @Override
    public List<LitemallBrand> selectLitemallBrandAll() {

        LitemallBrand litemallBrand=new LitemallBrand();
        return baseMapper.selectLitemallBrandList(litemallBrand);
    }

    /**
     * 新增品牌商
     *
     * @param litemallBrand 品牌商
     * @return 结果
     */
    @Override
    public int insertLitemallBrand(LitemallBrand litemallBrand)
    {
        litemallBrand.setAddTime(DateUtils.getNowDate());
        return baseMapper.insertLitemallBrand(litemallBrand);
    }

    /**
     * 修改品牌商
     *
     * @param litemallBrand 品牌商
     * @return 结果
     */
    @Override
    public int updateLitemallBrand(LitemallBrand litemallBrand)
    {
        litemallBrand.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallBrand(litemallBrand);
    }

    /**
     * 批量删除品牌商
     *
     * @param ids 需要删除的品牌商ID
     * @return 结果
     */
    @Override
    public int deleteLitemallBrandByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallBrandByIds(ids);
    }

    /**
     * 删除品牌商信息,逻辑删除
     *
     * @param id 品牌商ID
     * @return 结果
     */
    @Override
    public int deleteLitemallBrandById(Long id)
    {
        LitemallBrand litemallBrand = baseMapper.selectLitemallBrandById(id);

        if (litemallBrand.getDeleted()==1){
            litemallBrand.setDeleted(0);
            litemallBrand.setUpdateTime(DateUtils.getNowDate());
            return baseMapper.updateLitemallBrand(litemallBrand);
        }
        return baseMapper.deleteLitemallBrandById(id);
    }
}
