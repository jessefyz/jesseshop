package com.jessefyz.module.shop.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.constants.CouponConstant;
import com.jessefyz.module.shop.domain.LitemallCoupon;
import com.jessefyz.module.shop.mapper.LitemallCouponMapper;
import com.jessefyz.module.shop.service.ILitemallCouponService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 优惠券信息及规则Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallCouponServiceImpl extends ServiceImpl<LitemallCouponMapper, LitemallCoupon> implements ILitemallCouponService
{
    /**
     * 查询优惠券信息及规则
     *
     * @param id 优惠券信息及规则ID
     * @return 优惠券信息及规则
     */
    @Override
    public LitemallCoupon selectLitemallCouponById(Long id)
    {
        return baseMapper.selectLitemallCouponById(id);
    }

    /**
     * 查询优惠券信息及规则列表
     *
     * @param litemallCoupon 优惠券信息及规则
     * @return 优惠券信息及规则
     */
    @Override
    public List<LitemallCoupon> selectLitemallCouponList(LitemallCoupon litemallCoupon)
    {
        return baseMapper.selectLitemallCouponList(litemallCoupon);
    }

    /**
     * 新增优惠券信息及规则
     *
     * @param litemallCoupon 优惠券信息及规则
     * @return 结果
     */
    @Override
    public int insertLitemallCoupon(LitemallCoupon litemallCoupon)
    {
        return baseMapper.insertLitemallCoupon(litemallCoupon);
    }

    /**
     * 修改优惠券信息及规则
     *
     * @param litemallCoupon 优惠券信息及规则
     * @return 结果
     */
    @Override
    public int updateLitemallCoupon(LitemallCoupon litemallCoupon)
    {
        litemallCoupon.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallCoupon(litemallCoupon);
    }

    /**
     * 批量删除优惠券信息及规则
     *
     * @param ids 需要删除的优惠券信息及规则ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCouponByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallCouponByIds(ids);
    }

    /**
     * 删除优惠券信息及规则信息
     *
     * @param id 优惠券信息及规则ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCouponById(Long id)
    {
        return baseMapper.deleteLitemallCouponById(id);
    }


    /**
     * 查询过期的优惠券:
     * 注意：如果timeType=0, 即基于领取时间有效期的优惠券，则优惠券不会过期
     *
     * @return
     */
    @Override
    public List<LitemallCoupon> queryExpired() {
        return this.list(new LambdaQueryWrapper<LitemallCoupon>()
            .eq(LitemallCoupon::getStatus,CouponConstant.STATUS_NORMAL)
            .eq(LitemallCoupon::getTimeType,CouponConstant.TIME_TYPE_TIME)
            .lt(LitemallCoupon::getEndTime,DateUtil.format(new Date(),DateUtils.YYYY_MM_DD_HH_MM_SS))
        );
    }

}
