package com.jessefyz.module.shop.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.constants.CouponUserConstant;
import com.jessefyz.module.shop.domain.LitemallCouponUser;
import com.jessefyz.module.shop.mapper.LitemallCouponUserMapper;
import com.jessefyz.module.shop.service.ILitemallCouponUserService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 优惠券用户使用Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallCouponUserServiceImpl extends ServiceImpl<LitemallCouponUserMapper, LitemallCouponUser> implements ILitemallCouponUserService
{

    /**
     * 查询优惠券用户使用
     *
     * @param id 优惠券用户使用ID
     * @return 优惠券用户使用
     */
    @Override
    public LitemallCouponUser selectLitemallCouponUserById(Long id)
    {
        return baseMapper.selectLitemallCouponUserById(id);
    }

    /**
     * 查询优惠券用户使用列表
     *
     * @param litemallCouponUser 优惠券用户使用
     * @return 优惠券用户使用
     */
    @Override
    public List<LitemallCouponUser> selectLitemallCouponUserList(LitemallCouponUser litemallCouponUser)
    {
        return baseMapper.selectLitemallCouponUserList(litemallCouponUser);
    }

    /**
     * 新增优惠券用户使用
     *
     * @param litemallCouponUser 优惠券用户使用
     * @return 结果
     */
    @Override
    public int insertLitemallCouponUser(LitemallCouponUser litemallCouponUser)
    {
        return baseMapper.insertLitemallCouponUser(litemallCouponUser);
    }

    /**
     * 修改优惠券用户使用
     *
     * @param litemallCouponUser 优惠券用户使用
     * @return 结果
     */
    @Override
    public int updateLitemallCouponUser(LitemallCouponUser litemallCouponUser)
    {
        litemallCouponUser.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallCouponUser(litemallCouponUser);
    }

    /**
     * 批量删除优惠券用户使用
     *
     * @param ids 需要删除的优惠券用户使用ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCouponUserByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallCouponUserByIds(ids);
    }

    /**
     * 删除优惠券用户使用信息
     *
     * @param id 优惠券用户使用ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCouponUserById(Long id)
    {
        return baseMapper.deleteLitemallCouponUserById(id);
    }

    @Override
    public List<LitemallCouponUser> findByOid(Integer orderId) {
        return this.list(new LambdaQueryWrapper<LitemallCouponUser>()
            .eq(LitemallCouponUser::getOrderId,orderId)
        );
    }

    @Override
    public List<LitemallCouponUser> queryExpired() {
        return this.list(new LambdaQueryWrapper<LitemallCouponUser>()
            .eq(LitemallCouponUser::getStatus,CouponUserConstant.STATUS_USABLE)
                .lt(LitemallCouponUser::getEndTime, DateUtil.format(new Date(),DateUtils.YYYY_MM_DD_HH_MM_SS))
        );
    }
}
