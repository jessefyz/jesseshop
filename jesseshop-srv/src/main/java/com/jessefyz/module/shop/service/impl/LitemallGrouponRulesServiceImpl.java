package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.common.utils.GrouponConstant;
import com.jessefyz.module.shop.domain.LitemallGrouponRules;
import com.jessefyz.module.shop.mapper.LitemallGrouponRulesMapper;
import com.jessefyz.module.shop.service.ILitemallGrouponRulesService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 团购规则Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallGrouponRulesServiceImpl extends ServiceImpl<LitemallGrouponRulesMapper, LitemallGrouponRules> implements ILitemallGrouponRulesService
{

    /**
     * 查询团购规则
     *
     * @param id 团购规则ID
     * @return 团购规则
     */
    @Override
    public LitemallGrouponRules selectLitemallGrouponRulesById(Long id)
    {
        return baseMapper.selectLitemallGrouponRulesById(id);
    }

    /**
     * 查询团购规则列表
     *
     * @param litemallGrouponRules 团购规则
     * @return 团购规则
     */
    @Override
    public List<LitemallGrouponRules> selectLitemallGrouponRulesList(LitemallGrouponRules litemallGrouponRules)
    {
        return baseMapper.selectLitemallGrouponRulesList(litemallGrouponRules);
    }

    /**
     * 新增团购规则
     *
     * @param litemallGrouponRules 团购规则
     * @return 结果
     */
    @Override
    public int insertLitemallGrouponRules(LitemallGrouponRules litemallGrouponRules)
    {
        litemallGrouponRules.setAddTime(DateUtils.getNowDate());
        litemallGrouponRules.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.insertLitemallGrouponRules(litemallGrouponRules);
    }

    /**
     * 修改团购规则
     *
     * @param litemallGrouponRules 团购规则
     * @return 结果
     */
    @Override
    public int updateLitemallGrouponRules(LitemallGrouponRules litemallGrouponRules)
    {
        litemallGrouponRules.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallGrouponRules(litemallGrouponRules);
    }

    /**
     * 批量删除团购规则
     *
     * @param ids 需要删除的团购规则ID
     * @return 结果
     */
    @Override
    public int deleteLitemallGrouponRulesByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallGrouponRulesByIds(ids);
    }

    /**
     * 删除团购规则信息
     *
     * @param id 团购规则ID
     * @return 结果
     */
    @Override
    public int deleteLitemallGrouponRulesById(Long id)
    {
        return baseMapper.deleteLitemallGrouponRulesById(id);
    }

    @Override
    public List<LitemallGrouponRules> queryList(Integer page, Integer limit, String sort, String order) {
        PageHelper.startPage(page, limit);
        return this.list(new LambdaQueryWrapper<LitemallGrouponRules>()
            .eq(LitemallGrouponRules::getStatus,GrouponConstant.RULE_STATUS_ON)
        );
    }
}
