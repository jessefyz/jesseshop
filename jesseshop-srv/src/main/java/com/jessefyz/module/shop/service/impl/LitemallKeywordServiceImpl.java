package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallKeyword;
import com.jessefyz.module.shop.mapper.LitemallKeywordMapper;
import com.jessefyz.module.shop.service.ILitemallKeywordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 关键字Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallKeywordServiceImpl extends ServiceImpl<LitemallKeywordMapper,LitemallKeyword> implements ILitemallKeywordService
{

    /**
     * 查询关键字
     *
     * @param id 关键字ID
     * @return 关键字
     */
    @Override
    public LitemallKeyword selectLitemallKeywordById(Long id)
    {
        return baseMapper.selectLitemallKeywordById(id);
    }

    /**
     * 查询关键字列表
     *
     * @param litemallKeyword 关键字
     * @return 关键字
     */
    @Override
    public List<LitemallKeyword> selectLitemallKeywordList(LitemallKeyword litemallKeyword)
    {
        return baseMapper.selectLitemallKeywordList(litemallKeyword);
    }

    /**
     * 新增关键字
     *
     * @param litemallKeyword 关键字
     * @return 结果
     */
    @Override
    public int insertLitemallKeyword(LitemallKeyword litemallKeyword)
    {
        return baseMapper.insertLitemallKeyword(litemallKeyword);
    }

    /**
     * 修改关键字
     *
     * @param litemallKeyword 关键字
     * @return 结果
     */
    @Override
    public int updateLitemallKeyword(LitemallKeyword litemallKeyword)
    {
        litemallKeyword.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallKeyword(litemallKeyword);
    }

    /**
     * 批量删除关键字
     *
     * @param ids 需要删除的关键字ID
     * @return 结果
     */
    @Override
    public int deleteLitemallKeywordByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallKeywordByIds(ids);
    }

    /**
     * 删除关键字信息
     *
     * @param id 关键字ID
     * @return 结果
     */
    @Override
    public int deleteLitemallKeywordById(Long id)
    {
        return baseMapper.deleteLitemallKeywordById(id);
    }

    @Override
    public LitemallKeyword queryDefault() {
        return this.getOne(new LambdaQueryWrapper<LitemallKeyword>()
            .eq(LitemallKeyword::getIsDefault,1)
            ,false);
    }

    @Override
    public List<LitemallKeyword> queryHots() {
        return this.list(new LambdaQueryWrapper<LitemallKeyword>()
                        .eq(LitemallKeyword::getIsHot,1)
        );
    }

    @Override
    public List<LitemallKeyword> queryByKeyword(String keyword, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return this.list(new LambdaQueryWrapper<LitemallKeyword>()
            .like(LitemallKeyword::getKeyword,keyword)
        );
    }

}
