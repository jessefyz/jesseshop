package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallOrderGoods;
import com.jessefyz.module.shop.mapper.LitemallOrderGoodsMapper;
import com.jessefyz.module.shop.service.ILitemallOrderGoodsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单商品Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallOrderGoodsServiceImpl extends ServiceImpl<LitemallOrderGoodsMapper, LitemallOrderGoods> implements ILitemallOrderGoodsService
{

    /**
     * 查询订单商品
     *
     * @param id 订单商品ID
     * @return 订单商品
     */
    @Override
    public LitemallOrderGoods selectLitemallOrderGoodsById(Long id)
    {
        return baseMapper.selectLitemallOrderGoodsById(id);
    }

    /**
     * 查询订单商品列表
     *
     * @param litemallOrderGoods 订单商品
     * @return 订单商品
     */
    @Override
    public List<LitemallOrderGoods> selectLitemallOrderGoodsList(LitemallOrderGoods litemallOrderGoods)
    {
        return baseMapper.selectLitemallOrderGoodsList(litemallOrderGoods);
    }

    /**
     * 新增订单商品
     *
     * @param litemallOrderGoods 订单商品
     * @return 结果
     */
    @Override
    public int insertLitemallOrderGoods(LitemallOrderGoods litemallOrderGoods)
    {
        return baseMapper.insertLitemallOrderGoods(litemallOrderGoods);
    }

    /**
     * 修改订单商品
     *
     * @param litemallOrderGoods 订单商品
     * @return 结果
     */
    @Override
    public int updateLitemallOrderGoods(LitemallOrderGoods litemallOrderGoods)
    {
        litemallOrderGoods.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallOrderGoods(litemallOrderGoods);
    }

    /**
     * 批量删除订单商品
     *
     * @param ids 需要删除的订单商品ID
     * @return 结果
     */
    @Override
    public int deleteLitemallOrderGoodsByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallOrderGoodsByIds(ids);
    }

    /**
     * 删除订单商品信息
     *
     * @param id 订单商品ID
     * @return 结果
     */
    @Override
    public int deleteLitemallOrderGoodsById(Long id)
    {
        return baseMapper.deleteLitemallOrderGoodsById(id);
    }

    @Override
    public List<LitemallOrderGoods> queryByOid(Long orderId) {
        return this.list(new LambdaQueryWrapper<LitemallOrderGoods>()
            .eq(LitemallOrderGoods::getOrderId,orderId)
        );
    }

    @Override
    public Integer getComments(Integer orderId) {
        return this.count(new LambdaQueryWrapper<LitemallOrderGoods>()
            .eq(LitemallOrderGoods::getOrderId,orderId)
        );
    }

    @Override
    public List<LitemallOrderGoods> findByOidAndGid(Integer orderId, Integer goodsId) {
        return this.list(new LambdaQueryWrapper<LitemallOrderGoods>()
            .eq(LitemallOrderGoods::getOrderId,orderId)
            .eq(LitemallOrderGoods::getGoodsId,goodsId)
        );
    }
}
