package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallSearchHistory;
import com.jessefyz.module.shop.mapper.LitemallSearchHistoryMapper;
import com.jessefyz.module.shop.service.ILitemallSearchHistoryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 搜索历史Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallSearchHistoryServiceImpl extends ServiceImpl<LitemallSearchHistoryMapper,LitemallSearchHistory> implements ILitemallSearchHistoryService
{

    /**
     * 查询搜索历史
     *
     * @param id 搜索历史ID
     * @return 搜索历史
     */
    @Override
    public LitemallSearchHistory selectLitemallSearchHistoryById(Long id)
    {
        return baseMapper.selectLitemallSearchHistoryById(id);
    }

    /**
     * 查询搜索历史列表
     *
     * @param litemallSearchHistory 搜索历史
     * @return 搜索历史
     */
    @Override
    public List<LitemallSearchHistory> selectLitemallSearchHistoryList(LitemallSearchHistory litemallSearchHistory)
    {
        return baseMapper.selectLitemallSearchHistoryList(litemallSearchHistory);
    }

    /**
     * 新增搜索历史
     *
     * @param litemallSearchHistory 搜索历史
     * @return 结果
     */
    @Override
    public int insertLitemallSearchHistory(LitemallSearchHistory litemallSearchHistory)
    {
        return baseMapper.insertLitemallSearchHistory(litemallSearchHistory);
    }

    /**
     * 修改搜索历史
     *
     * @param litemallSearchHistory 搜索历史
     * @return 结果
     */
    @Override
    public int updateLitemallSearchHistory(LitemallSearchHistory litemallSearchHistory)
    {
        litemallSearchHistory.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallSearchHistory(litemallSearchHistory);
    }

    /**
     * 批量删除搜索历史
     *
     * @param ids 需要删除的搜索历史ID
     * @return 结果
     */
    @Override
    public int deleteLitemallSearchHistoryByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallSearchHistoryByIds(ids);
    }

    /**
     * 删除搜索历史信息
     *
     * @param id 搜索历史ID
     * @return 结果
     */
    @Override
    public int deleteLitemallSearchHistoryById(Long id)
    {
        return baseMapper.deleteLitemallSearchHistoryById(id);
    }

    @Override
    public void deleteByUid(Long uid) {
        this.remove(new LambdaQueryWrapper<LitemallSearchHistory>()
            .eq(LitemallSearchHistory::getUserId,uid)
        );
    }

    @Override
    public List<LitemallSearchHistory> queryByUid(Long uid) {
        return this.list(new LambdaQueryWrapper<LitemallSearchHistory>()
            .eq(LitemallSearchHistory::getUserId,uid)
        );
    }
}
