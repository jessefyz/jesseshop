package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallStorage;
import com.jessefyz.module.shop.mapper.LitemallStorageMapper;
import com.jessefyz.module.shop.service.ILitemallStorageService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 文件存储Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallStorageServiceImpl extends ServiceImpl<LitemallStorageMapper,LitemallStorage> implements ILitemallStorageService
{

    /**
     * 查询文件存储
     *
     * @param id 文件存储ID
     * @return 文件存储
     */
    @Override
    public LitemallStorage selectLitemallStorageById(Long id)
    {
        return baseMapper.selectLitemallStorageById(id);
    }

    /**
     * 查询文件存储列表
     *
     * @param litemallStorage 文件存储
     * @return 文件存储
     */
    @Override
    public List<LitemallStorage> selectLitemallStorageList(LitemallStorage litemallStorage)
    {
        return baseMapper.selectLitemallStorageList(litemallStorage);
    }

    @Override
    public LitemallStorage selectLitemallStorageByKey(LitemallStorage litemallStorage) {
        return baseMapper.selectLitemallStorageByKey(litemallStorage);
    }

    /**
     * 新增文件存储
     *
     * @param litemallStorage 文件存储
     * @return 结果
     */
    @Override
    public int insertLitemallStorage(LitemallStorage litemallStorage)
    {
        litemallStorage.setAddTime(new Date());
        litemallStorage.setUpdateTime(new Date());
        return baseMapper.insertLitemallStorage(litemallStorage);
    }

    /**
     * 修改文件存储
     *
     * @param litemallStorage 文件存储
     * @return 结果
     */
    @Override
    public int updateLitemallStorage(LitemallStorage litemallStorage)
    {
        litemallStorage.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallStorage(litemallStorage);
    }

    /**
     * 批量删除文件存储
     *
     * @param ids 需要删除的文件存储ID
     * @return 结果
     */
    @Override
    public int deleteLitemallStorageByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallStorageByIds(ids);
    }

    /**
     * 删除文件存储信息
     *
     * @param id 文件存储ID
     * @return 结果
     */
    @Override
    public int deleteLitemallStorageById(Long id)
    {
        return baseMapper.deleteLitemallStorageById(id);
    }

    @Override
    public LitemallStorage findByKey(String key) {
        return this.getOne(new LambdaQueryWrapper<LitemallStorage>()
                .eq(LitemallStorage::getKey,key)
                ,false);
    }
}
